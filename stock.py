#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
import datetime
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.pyson import Not, Equal, Eval
from trytond.pool import Pool


class ShipmentIn(ModelWorkflow, ModelSQL, ModelView):
    _name = 'stock.shipment.in'

    def __init__(self):
        super(ShipmentIn, self).__init__()
        shipment_in_obj = Pool().get('stock.shipment.in')
        self.effective_date = copy.copy(shipment_in_obj.effective_date)
        if self.effective_date.readonly!=None:
            self.effective_date.readonly = False
        if self.effective_date.states.has_key('states'):
            self.effective_date.states.update({'required': True})
        else:
            self.effective_date.states = {
                    'readonly': Not(Equal(Eval('state'), 'draft')),
                    'required': True}
        if 'state' not in self.effective_date.depends:
            self.effective_date.depends = copy.copy(
                self.effective_date.depends)
            self.effective_date.depends.append('state')

        self._reset_columns()

    def default_effective_date(self):
        return datetime.date.today()

    def set_state_received(self, shipment_id):
        move_obj = Pool().get('stock.move')
        shipment = self.browse(shipment_id)
        move_obj.write(
            [m.id for m in shipment.incoming_moves \
                 if m.state not in ('done', 'cancel')],
            {'state': 'done',
             'effective_date': shipment.effective_date
             })
        self.write(shipment_id, {
            'state': 'received'
            })

    def set_state_done(self, shipment_id):
        move_obj = Pool().get('stock.move')
        shipment = self.browse(shipment_id)
        move_obj.write(
            [m.id for m in shipment.inventory_moves \
                 if m.state not in ('done', 'cancel')],
            {'state': 'done',
             'effective_date': shipment.effective_date
             })
        self.write(shipment_id,{
            'state': 'done',
            })

ShipmentIn()


class Move(ModelSQL, ModelView):
    _name = 'stock.move'

    def write(self, ids, vals):
        if isinstance(ids, (int, long)):
            ids = [ids]

        vls = vals.copy()
        res = super(Move, self).write(ids, vals)

        if 'effective_date' in vls and 'state' in vls:
            del vls['state']
            res = super(Move, self).write(ids, vls)
        return res

Move()
